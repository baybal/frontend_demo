	describe('InitFactory', function() {

		var InitFactory;

		beforeEach(function() {
			angular.module('demoApp');
		});

		beforeEach(inject(function() {
			var $injector = angular.injector(['demoApp']);
			InitFactory = $injector.get('InitFactory');
		}));

		it('InitFactory must return true upon correct execution', function() {
			InitFactory.toBeTruthy();
		});

		it("should support async execution of test preparation and expectations", function(done) {
			expect(GlobalConfigValue.backendURL && GlobalConfigValue.dataLocation && GlobalConfigValue.BECallSetup).toBeTruthy();
			done();
		});

	});