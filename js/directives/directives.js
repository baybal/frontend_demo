demoApp.directive('employee', function() {
	return {
		restrict: 'A', // only activate on element attribute
		scope: {
			item: "=",
		},
		link: function(scope){
			scope.alert = function(i){alert(i)};
		},
		templateUrl: '/js/directives/employee'
	};
});