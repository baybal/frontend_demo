var debugAnchor;

demoApp.controller('MainController', [
	'$rootScope',
	'EmployeeListFactory',
	function(
		$rootScope,
		EmployeeListFactory
	) {
		this.employeeList = [];
		$rootScope.$on('employeeListUpdated', (function() {
			angular.copy(EmployeeListFactory, this.employeeList);
		}).bind(this));
}]);
