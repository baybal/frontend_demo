demoApp.factory('EmployeeListFactory', [
	'$http',
	'GlobalConfigValue',
	'GlobalParamsValue',
	'$rootScope',
	'InitFactory',
	function(
		$http,
		GlobalConfigValue,
		GlobalParamsValue,
		$rootScope,
		InitFactory
	) {
		var factory = [];
		var personProto = {
			toggle: function(item) {
				!this.toggled && factory.forEach(function(i2) {
					i2.toggled = false;
				});
				this.toggled = !this.toggled;
				$rootScope.$emit('employeeListUpdated');
			}
		};
		GlobalParamsValue.backendRetries = 0;

		function BECall() {
			$http.get(GlobalConfigValue.dataLocation, GlobalConfigValue.BECallSetup).then(
				function(response) {
					angular.copy(response.data, factory);
					factory.forEach(function(i) {
						Object.setPrototypeOf(i, personProto);
						i.toggle = i.toggle.bind(i);
					});
					$rootScope.$emit('employeeListUpdated');
				},
				function(response) {
					if (response.status === -1) {
						if (GlobalParamsValue.backendRetries < 3) {
							GlobalParamsValue.backendRetries++;
							BECall();
						}
						else {
							GlobalParamsValue.BEError = 'Backend call failure: timeout. EmployeeListFactory.';
							console.error(GlobalParamsValue.BEError);
						}
					}
					else {
						GlobalParamsValue.BEError = 'Error: ' + response;
						console.error(GlobalParamsValue.BEError);
					}
				}
			);
		}
		$rootScope.$on('stage3', BECall);

		return factory;
}]);
