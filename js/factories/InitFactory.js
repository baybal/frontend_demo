demoApp.factory('InitFactory', [
	'$http',
	'GlobalConfigValue',
	'GlobalParamsValue',
	'$rootScope',
	function(
		$http,
		GlobalConfigValue,
		GlobalParamsValue,
		$rootScope
	) {

		return (function() {
			//Starting with config retrieval
			(function stage1() {
				$http.get(GlobalConfigValue.configurl, {
					timeout: 3000
				}).then(
					function(response) {
						angular.merge(GlobalConfigValue, response.data);
						GlobalParamsValue.backendRetries = 0;
						stage2();
					},
					function(response) {
						if (response.status === -1) {
							if (GlobalParamsValue.backendRetries < 3) {
								GlobalParamsValue.backendRetries++;
								stage1();
							}
							else {
								GlobalParamsValue.BEError = 'Backend call failure: timeout. EmployeeListFactory.';
							}
						}
						else {
							GlobalParamsValue.BEError = 'Error: ' + response.statusText;
						}
					}
				);
			})();

			function stage2() {
				window.debugHarness.GlobalConfigValue = GlobalConfigValue;
				window.debugHarness.GlobalParamsValue = GlobalParamsValue;
				window.debugHarness.rootScope = $rootScope;
				$rootScope.$emit('stage3');
			}

			return true;
		})();
}]);
