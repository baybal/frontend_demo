start: package.json prepare optimize_css minify_js

prepare:
	npm i

optimize_css: css/main.css
	node_modules/postcss-cli/bin/postcss css/main.css -o css/main.min.css -m css/main.min.css.map

minify_js: js/lib/angular.js js/directives/directives.js js/app.js js/factories/InitFactory.js js/factories/EmployeeListFactory.js js/controllers/MainController.js
	./node_modules/uglify-js/bin/uglifyjs js/lib/angular.js js/app.js js/factories/InitFactory.js  js/factories/EmployeeListFactory.js js/directives/directives.js js/controllers/MainController.js -o js/main.min.js --lint --source-map js/main.min.js.map -c -m -p --source-map-url main.min.js.map

clean:
	rm -rf node_modules/

test:
	node_modules/karma/bin/karma start test/karma.conf